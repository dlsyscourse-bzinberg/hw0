#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <cmath>
#include <iostream>

namespace py = pybind11;


void mat_mat_subtract (float* x, const float* y, const size_t m, const size_t n) {
    /**
     * In-place subtracts `y` from `x` (i.e., mutates `x`).  Both `x` and
     * `y` are `m`-by-`n` matrices.
     */
    for (size_t i = 0; i < m * n; i++) {
        x[i] -= y[i];
    }
}

void scale_mat(float* x, const size_t m, const size_t n, float a) {
    /**
     * In-place scales the `m`-by-`n` matrix `x` by the scalar `a`.
     */
    for (size_t i = 0; i < m * n; i++) {
        x[i] *= a;
    }
}

void exp_all(float* x, const size_t m, const size_t n) {
    /**
     * In-place exponentiates each entry of the `m`-by-`n` matrix `x`.
     */
    for (size_t i = 0; i < m * n; i++) {
        x[i] = std::exp(x[i]);
    }
}

void normalize_rows(float* x, const size_t m, const size_t n) {
    /**
     * In-place normalizes (i.e. divides by a scalar) each row of the `m`-by`n`
     * matrix `x`.  After running this function, each row of `x` sums to 1.
     */
    for (size_t i = 0; i < m; i++) {
        float row_sum = 0;
        for (size_t j = 0; j < n; j++) {
            row_sum += x[i * n + j];
        }
        for (size_t j = 0; j < n; j++) {
            x[i * n + j] /= row_sum;
        }
    }
}

void subtract_ones(float* x, const size_t m, const size_t n,
                   const unsigned char* col_inds,
                   const size_t col_ind_istart) {
    /**
     * In-place subtracts 1 from exactly one entry in each row of the `m` by
     * `n` matrix `x`.  The column index of the entry that gets subtracted from
     * in row `i` is `col_inds[col_ind_istart + i]`.  Requires that all entries
     * of `col_inds` are less than `n`.
     */
    for (size_t i = 0; i < m; i++) {
        const auto col_ind = col_inds[col_ind_istart + i];
        assert (col_ind < n);
        x[i * n + col_ind] -= 1;
    }
}

void matmul_first_rowslice(const float x[], const float y[],
                           const size_t m, const size_t n, const size_t p,
                           const size_t i_start, const size_t i_end,
                           float z[]) {
    /**
     * Matrix-multiplies a slice of `x` by all of `y`, storing the result in
     * `z`.  The slice of `x` under consideration consists of rows `i_start`
     * through `i_end` (inclusive on the left and exclusive on the right).
     *
     * Here `x` is an `m`-by-`n` matrix and `y` is an `n`-by-`p` matrix.  Thus
     * `z` is an `(i_end - i_start)`-by-`p` matrix.
     */
    const auto m_slice = i_end - i_start;
    for (size_t i_offset = 0; i_offset < m_slice; i_offset++) {
        for (size_t k = 0; k < p; k++) {
            auto& entry = z[i_offset * p + k];
            entry = 0;
            for (size_t j = 0; j < n; j++) {
                entry += x[(i_start + i_offset) * n + j] * y[j * p + k];
            }
        }
    }
}

void matmul_first_rowslice_transpose(const float x[], const float y[],
                                     const size_t m, const size_t n, const size_t p,
                                     const size_t i_start, const size_t i_end,
                                     float z[]) {
    /**
     * Matrix-multiplies a slice of `transpose(x)` by all of `y`, storing the
     * result in `z`.  The slice of `transpose(x)` under consideration consists
     * of columns `i_start` through `i_end` (inclusive on the left and
     * exclusive on the right).  To either clarify or further confuse, this
     * column-slice of `transpose(x)` could equivalently be described as the
     * transpose of a row-slice of `x`.
     *
     * Here `x` is an `m`-by-`n` matrix and `y` is an `(i_end - i_start)` by
     * `p` matrix.  Thus `z` is an `n`-by-`p` matrix.
     */
    const auto m_slice = i_end - i_start;
    for (size_t j = 0; j < n; j++) {
        for (size_t k = 0; k < p; k++) {
            auto& entry = z[j * p + k];
            entry = 0;
            for (size_t i_offset = 0; i_offset < m_slice; i_offset++) {
                entry += x[(i_start + i_offset) * n + j] * y[i_offset * p + k];
            }
        }
    }
}

void softmax_regression_epoch_cpp(const float *X, const unsigned char *y,
                                  float *theta, size_t m, size_t n, size_t k,
                                  float lr, size_t batch)
{
    /**
     * A C++ version of the softmax regression epoch code.  This should run a
     * single epoch over the data defined by X and y (and sizes m,n,k), and
     * modify theta in place.  Your function will probably want to allocate
     * (and then delete) some helper arrays to store the logits and gradients.
     *
     * Args:
     *     X (const float *): pointer to X data, of size m*n, stored in row
     *          major (C) format
     *     y (const unsigned char *): pointer to y data, of size m
     *     theta (float *): pointer to theta data, of size n*k, stored in row
     *          major (C) format
     *     m (size_t): number of examples
     *     n (size_t): input dimension
     *     k (size_t): number of classes
     *     lr (float): learning rate / SGD step size
     *     batch (int): SGD minibatch size
     *
     * Returns:
     *     (None)
     */
    assert(m % batch == 0);

    auto* ZZ = new float[batch * k];
    auto* scaled_theta_grad = new float[n * k];
    for (size_t i_start = 0; i_start < m; i_start += batch) {
        const size_t i_end = i_start + batch;
        // ZZ = X_mini @ theta
        matmul_first_rowslice(X, theta, m, n, k, i_start, i_end, ZZ);
        // ZZ = exp(ZZ)
        exp_all(ZZ, batch, k);
        // ZZ = _normalize(ZZ, axis=1)
        normalize_rows(ZZ, batch, k);
        // ZZ -= I_y
        subtract_ones(ZZ, batch, k, y, i_start);
        // scaled_theta_grad = X_mini.T @ ZZ
        matmul_first_rowslice_transpose(X, ZZ, m, n, k, i_start, i_end,
                                        scaled_theta_grad);
        // scaled_theta_grad *= lr / batch
        scale_mat(scaled_theta_grad, n, k, lr / batch);
        // theta -= scaled_theta_grad
        mat_mat_subtract(theta, scaled_theta_grad, n, k);
    }
    delete[] ZZ;
    delete[] scaled_theta_grad;
}


/**
 * This is the pybind11 code that wraps the function above.  It's only role is
 * wrap the function above in a Python module, and you do not need to make any
 * edits to the code
 */
PYBIND11_MODULE(simple_ml_ext, m) {
    m.def("softmax_regression_epoch_cpp",
    	[](py::array_t<float, py::array::c_style> X,
           py::array_t<unsigned char, py::array::c_style> y,
           py::array_t<float, py::array::c_style> theta,
           float lr,
           int batch) {
        softmax_regression_epoch_cpp(
        	static_cast<const float*>(X.request().ptr),
            static_cast<const unsigned char*>(y.request().ptr),
            static_cast<float*>(theta.request().ptr),
            X.request().shape[0],
            X.request().shape[1],
            theta.request().shape[1],
            lr,
            batch
           );
    },
    py::arg("X"), py::arg("y"), py::arg("theta"),
    py::arg("lr"), py::arg("batch"));
}
