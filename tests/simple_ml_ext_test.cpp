#define BOOST_TEST_MODULE hw0 test
#include <boost/test/included/unit_test.hpp>

#include "../src/simple_ml_ext.cpp"

BOOST_AUTO_TEST_CASE(test_mat_mat_sub) {
    float x[] = {  0,  1,  2,
                  10, 11, 12 };

    float y[] = { 100, 200, 300,
                  101, 201, 301 };

    mat_mat_subtract(x, y, 2, 3);
    float x_expected[] = { -100, -199, -298,
                            -91, -190, -289 };
    BOOST_CHECK_EQUAL_COLLECTIONS(x, &x[2 * 3],
                                  x_expected, &x_expected[2 * 3]);
}

BOOST_AUTO_TEST_CASE(test_scale_mat) {
    float x[] = {  0,  1,  2,
                  10, 11, 12 };
    float a = 7;

    scale_mat(x, 2, 3, a);
    float x_expected[] = {  0,  7, 14,
                           70, 77, 84 };
    BOOST_CHECK_EQUAL_COLLECTIONS(x, &x[2 * 3],
                                  x_expected, &x_expected[2 * 3]);
}

BOOST_AUTO_TEST_CASE(test_normalize_rows) {
    float x[] = {  0,  1,  2,
                  10, 11, 12 };

    normalize_rows(x, 2, 3);

    float x_expected[] = {        0,   1/3.0f,   2/3.0f,
                           10/33.0f, 11/33.0f, 12/33.0f };
    BOOST_CHECK_EQUAL_COLLECTIONS(x, &x[2 * 3],
                                  x_expected, &x_expected[2 * 3]);
}

BOOST_AUTO_TEST_CASE(test_subtract_ones) {
    float x[] = {  0,  1,  2,
                  10, 11, 12 };
    const unsigned char col_inds[] = { 0, 1, 1, 2,
                                       2, 0,  // <-- the relevant entries
                                       1, 0 };

    subtract_ones(x, 2, 3, col_inds, 4);

    float x_expected[] = { 0,  1,  1,
                           9, 11, 12 };
    BOOST_CHECK_EQUAL_COLLECTIONS(x, &x[2 * 3],
                                  x_expected, &x_expected[2 * 3]);
}

BOOST_AUTO_TEST_CASE(test_matmul_first_rowslice) {
    float x[] = {  0,  1,  2,
                  10, 11, 12,
                  20, 21, 22,
                  30, 31, 32 };
    // take just the middle two rows of `x`
    const size_t i_start = 1;
    const size_t i_end = 3;

    float y[] = { 0, 1,  2,  3,
                  4, 5,  6,  7,
                  8, 9, 10, 11 };

    float z[2 * 4];
    matmul_first_rowslice(x, y, 2, 3, 4, i_start, i_end, z);

    float z_expected[] = { 140, 173, 206, 239,
                           260, 323, 386, 449 };
    BOOST_CHECK_EQUAL_COLLECTIONS(z, &z[2 * 4],
                                  z_expected, &z_expected[2 * 4]);
}

BOOST_AUTO_TEST_CASE(test_matmul_first_rowslice_transpose) {
    float x[] = {  0,  1,  2,
                  10, 11, 12,
                  20, 21, 22,
                  30, 31, 32 };
    // take just the middle two rows of `x` (the middle to columns of
    // `transpose(x)`)
    const size_t i_start = 1;
    const size_t i_end = 3;

    float y[] = { 0, 1, 2,
                  3, 4, 5 };

    float z[3 * 3];
    matmul_first_rowslice_transpose(x, y, 4, 3, 3, i_start, i_end, z);

    float z_expected[] = { 60,  90, 120,
                           63,  95, 127,
                           66, 100, 134 };
    BOOST_CHECK_EQUAL_COLLECTIONS(z, &z[3 * 3],
                                  z_expected, &z_expected[3 * 3]);
}

template <typename T>
void print_matrix(const T* x, size_t m, size_t n) {
    for (size_t i = 0; i < m; i++) {
        for (size_t j = 0; j < n; j++) {
            std::cout << x[i*n + j] << ", ";
        }
        std::cout << std::endl;
    }
}

// Printf debugging side-by-side with the same printfs from the Python
// implementation.  (I found the bug this way!)
BOOST_AUTO_TEST_CASE(test_stepthrough) {
    const size_t num_examples = 50;
    const size_t input_dim = 5;
    const size_t num_classes = 3;

    auto X = new const float[] {
       1.7640524 ,  0.4001572 ,  0.978738  ,  2.2408931 ,  1.867558  ,
      -0.9772779 ,  0.95008844, -0.1513572 , -0.10321885,  0.41059852,
       0.14404356,  1.4542735 ,  0.7610377 ,  0.12167501,  0.44386324,
       0.33367434,  1.4940791 , -0.20515826,  0.3130677 , -0.85409576,
      -2.5529897 ,  0.6536186 ,  0.8644362 , -0.742165  ,  2.2697546 ,
      -1.4543657 ,  0.04575852, -0.18718386,  1.5327792 ,  1.4693588 ,
       0.15494743,  0.37816253, -0.88778573, -1.9807965 , -0.34791216,
       0.15634897,  1.2302907 ,  1.2023798 , -0.3873268 , -0.30230275,
      -1.048553  , -1.420018  , -1.7062702 ,  1.9507754 , -0.5096522 ,
      -0.4380743 , -1.2527953 ,  0.7774904 , -1.6138978 , -0.21274029,
      -0.89546657,  0.3869025 , -0.51080513, -1.1806322 , -0.02818223,
       0.42833188,  0.06651722,  0.3024719 , -0.6343221 , -0.36274117,
      -0.67246044, -0.35955316, -0.8131463 , -1.7262826 ,  0.17742614,
      -0.40178093, -1.6301984 ,  0.46278226, -0.9072984 ,  0.0519454 ,
       0.7290906 ,  0.12898292,  1.1394007 , -1.2348258 ,  0.40234163,
      -0.6848101 , -0.87079716, -0.5788497 , -0.31155252,  0.05616534,
      -1.1651498 ,  0.9008265 ,  0.46566245, -1.5362437 ,  1.4882522 ,
       1.8958892 ,  1.1787796 , -0.17992483, -1.0707526 ,  1.0544517 ,
      -0.40317693,  1.222445  ,  0.20827498,  0.97663903,  0.3563664 ,
       0.7065732 ,  0.01050002,  1.7858706 ,  0.12691209,  0.40198937,
       1.8831507 , -1.347759  , -1.270485  ,  0.9693967 , -1.1731234 ,
       1.9436212 , -0.41361898, -0.7474548 ,  1.922942  ,  1.4805148 ,
       1.867559  ,  0.90604466, -0.86122566,  1.9100649 , -0.26800337,
       0.8024564 ,  0.947252  , -0.15501009,  0.61407936,  0.9222067 ,
       0.37642553, -1.0994008 ,  0.2982382 ,  1.3263859 , -0.69456786,
      -0.14963454, -0.43515354,  1.8492638 ,  0.67229474,  0.40746182,
      -0.76991606,  0.5392492 , -0.6743327 ,  0.03183056, -0.6358461 ,
       0.67643327,  0.57659084, -0.20829876,  0.3960067 , -1.0930616 ,
      -1.4912575 ,  0.4393917 ,  0.1666735 ,  0.63503146,  2.3831449 ,
       0.94447947, -0.91282225,  1.1170163 , -1.3159074 , -0.4615846 ,
      -0.0682416 ,  1.7133427 , -0.74475485, -0.82643855, -0.09845252,
      -0.6634783 ,  1.1266359 , -1.0799315 , -1.1474687 , -0.43782005,
      -0.49803245,  1.929532  ,  0.9494208 ,  0.08755124, -1.2254355 ,
       0.844363  , -1.0002153 , -1.5447711 ,  1.1880298 ,  0.3169426 ,
       0.9208588 ,  0.31872764,  0.8568306 , -0.6510256 , -1.0342429 ,
       0.6815945 , -0.80340964, -0.6895498 , -0.4555325 ,  0.01747916,
      -0.35399392, -1.3749512 , -0.6436184 , -2.2234032 ,  0.62523144,
      -1.6020577 , -1.1043833 ,  0.05216508, -0.739563  ,  1.5430146 ,
      -1.2928569 ,  0.26705086, -0.03928282, -1.1680934 ,  0.5232767 ,
      -0.17154633,  0.77179056,  0.82350415,  2.163236  ,  1.336528  ,
      -0.36918184, -0.23937918,  1.0996596 ,  0.6552637 ,  0.64013153,
      -1.616956  , -0.02432613, -0.7380309 ,  0.2799246 , -0.09815039,
       0.9101789 ,  0.3172182 ,  0.78632796, -0.4664191 , -0.94444627,
      -0.4100497 , -0.01702041,  0.37915173,  2.259309  , -0.04225715,
      -0.955945  , -0.34598178, -0.463596  ,  0.48148146, -1.540797  ,
       0.06326199,  0.15650654,  0.23218104, -0.5973161 , -0.23792173,
      -1.424061  , -0.49331987, -0.54286146,  0.41605005, -1.1561824 ,
       0.7811981 ,  1.4944845 , -2.069985  ,  0.42625874,  0.676908  ,
      -0.63743705, -0.3972718 , -0.13288058, -0.29779088, -0.30901298,
      -1.6760038 ,  1.1523316 ,  1.0796186 , -0.81336427, -1.4664243 ,
    };

    auto y = new const unsigned char[] {
        1, 0, 2, 0, 0, 1, 2, 1, 1, 1, 0, 0, 2, 0, 0, 2, 1, 2, 2, 0, 2, 2, 0, 2,
        1, 0, 1, 2, 1, 1, 0, 1, 1, 1, 2, 0, 1, 1, 0, 1, 2, 0, 1, 2, 1, 2, 1, 2,
        1, 2
    };

    auto theta = new float[] { 0, 0, 0,
                               0, 0, 0,
                               0, 0, 0,
                               0, 0, 0,
                               0, 0, 0 };

    const auto m = num_examples;
    const auto n = input_dim;
    const auto k = num_classes;

    const float lr = 1.0;
    const size_t batch = 50;

    auto* ZZ = new float[batch * k];
    auto* scaled_theta_grad = new float[n * k];
    for (size_t i_start = 0; i_start < m; i_start += batch) {
        const size_t i_end = i_start + batch;
        matmul_first_rowslice(X, theta, m, n, k, i_start, i_end, ZZ);

        std::cout << "Before exp:" << std::endl;
        print_matrix(ZZ, batch, k);

        exp_all(ZZ, batch, k);

        std::cout << "After exp:" << std::endl;
        print_matrix(ZZ, batch, k);

        normalize_rows(ZZ, batch, k);

        std::cout << "After normalize:" << std::endl;
        print_matrix(ZZ, batch, k);

        subtract_ones(ZZ, batch, k, y, i_start);

        std::cout << "After subtracting onehots:" << std::endl;
        print_matrix(ZZ, batch, k);

        matmul_first_rowslice_transpose(X, ZZ, m, n, k, i_start, i_end, scaled_theta_grad);

        std::cout << "After transpose matmul:" << std::endl;
        print_matrix(scaled_theta_grad, batch, k);

        scale_mat(scaled_theta_grad, n, k, lr / batch);

        std::cout << "Scaled grad wrt theta:" << std::endl;
        print_matrix(scaled_theta_grad, n, k);

        mat_mat_subtract(theta, scaled_theta_grad, n, k);

        std::cout << "Theta at the end of loop iteration:" << std::endl;
        print_matrix(theta, n, k);
    }
    delete[] ZZ;
    delete[] scaled_theta_grad;
}
